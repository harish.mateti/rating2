var obj = {
    "avgRatings": 3.2,
    "ratingsCount": 48,
    "ratingDetails": [{
        "text": "5",
        "avgRatingsPercentage": 70
    },
    {
        "text": "4",
        "avgRatingsPercentage": 13
    },
    {
        "text": "3",
        "avgRatingsPercentage": 5
    },
    {
        "text": "2",
        "avgRatingsPercentage": 2
    }, {
        "text": "1",
        "avgRatingsPercentage": 10
    }


    ]
};
var script = document.createElement('script');
script.src = "https://kit.fontawesome.com/9d091bc651.js";

document.head.appendChild(script);

var rating = document.getElementById("rating");
rating.style.cssText = "border: 1px solid #D4D5D9;width: 300px;padding: 5px 15px;"

function totalrating(heading, ratingcount) {
    var h1 = document.createElement(heading);
    h1.innerHTML = ratingcount;
    h1.style.cssText = "display:inline;border-right: 3px solid #D4D5D9;padding-right:20px;"
    rating.appendChild(h1);
    var span = document.createElement("span");
    span.className = "fas fa-star";
    span.style.cssText = "font-size:20px;margin-left:10px;position: relative;top: -3;color: #14958f"
    h1.appendChild(span);
}
totalrating("h1", obj.avgRatings);


function totalnumber(heading, ratingnum) {
    var ratinghead = document.createElement(heading);
    ratinghead.style.cssText = "display:inline;margin-left:15px;font-weight:lighter;"
    ratinghead.innerHTML = `${ratingnum} Ratings`;
    rating.appendChild(ratinghead);
}
totalnumber("h1", obj.ratingsCount);
